all: docker_down docker_build deploy test_app_k8s
docker_down: 
			cd docker && docker-compose down 

docker_build:
			cd docker && docker-compose up -d --build --force-recreate && cd ..

deploy:
			kubectl apply -f kubernetes

test_app_k8s:
			kubectl expose deployment reto-devops-deployment --type=NodePort --port=80 && minikube service reto-devops-service --url