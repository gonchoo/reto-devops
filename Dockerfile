#Seleccionamos imagen alpine3.11 por su bajo tamaño.
FROM node:erbium-alpine3.11

#Ejecutamos el siguiente comando para añadir el grupo y usuario definidos, 
#luego creamos el directorio para nuestra aplicacion y otorgamos permisos a nuestro usuario
RUN	addgroup -S appgroup && adduser -s /bin/bash -S -G appgroup gonzalo && \
mkdir -p /var/app/ && chown gonzalo:appgroup /var/app/

#Definimos nuestro directorio de trabajo 
WORKDIR /var/app/

#Seleccionamos nuestro usuario distinto de root
USER gonzalo

#Copiamos la aplicacion de nodejs al workdir (/var/app/) excluyendo los archivos indicados en .dockerignore
COPY ./ ./

#Se remueve la compilación ya que esta la realiza el gitlab-ci
#Exponemos el puerto 3000 para nuestra app
EXPOSE 3000

#Definimos lo que mantiene vivo al contenedor
ENTRYPOINT ["node", "index.js"]